export default interface Action<P, D> {
  type: string;
  payload?: P;
  data?: D;
  error?: string;
}
