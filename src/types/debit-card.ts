export type DebitCard = {
  availableBalance: number;
  cardholderName: string;
  cardNumber: number;
  expiry: string;
  cvv: number;
  cardType: 'visa' | 'master';
  spendingLimit: number;
  spent: number;
  isLimitSet: boolean;
};
