import {DebitCard} from '../types/debit-card';
import {API_BASE_URL} from '@env';

export const getDebitCard = async (): Promise<DebitCard | {}> => {
  try {
    const response = await fetch(API_BASE_URL + '/debitCard');
    if (!response.ok) {
      throw new Error('Cannot get data');
    }
    return response.json();
  } catch (error) {
    throw error;
  }
};

export const setWeeklyLimit = async (
  card: DebitCard,
): Promise<DebitCard | {}> => {
  try {
    const requestOptions = {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(card),
    };
    const response = await fetch(API_BASE_URL + '/debitCard', requestOptions);
    if (!response.ok) {
      throw new Error('Cannot update data');
    }
    return response.json();
  } catch (error) {
    throw error;
  }
};

export const getLimits = async (): Promise<Array<number> | {}> => {
  try {
    const response = await fetch(API_BASE_URL + '/weeklylimits');
    if (!response.ok) {
      throw new Error('Cannot get data');
    }
    return response.json();
  } catch (error) {
    throw error;
  }
};
