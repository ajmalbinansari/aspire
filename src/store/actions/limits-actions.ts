import {DebitCard} from '../../types/debit-card';
import Action from '../../types/redux-types';
import * as ActionTypes from './action-types';
export const getLimits = (): Action<null, Array<number>> => ({
  type: ActionTypes.GET_LIMITS,
});
export const updateLimit = (
  card: DebitCard,
): Action<DebitCard, Array<number>> => ({
  type: ActionTypes.SET_LIMIT,
  payload: card,
});
