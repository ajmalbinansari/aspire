import {DebitCard} from '../../types/debit-card';
import Action from '../../types/redux-types';
import {APIStatus, AppState} from '../reducers/app-state';
import * as ActionTypes from './action-types';
export const getDebitCard = (): Action<null, DebitCard> => ({
  type: ActionTypes.GET_DEBIT_CARD,
});
export const debitCardFetched = (
  debitCard: DebitCard,
): Action<{}, DebitCard> => ({
  type: ActionTypes.DEBIT_CARD_FETCHED,
  payload: debitCard,
});

export const setUpdateApiIdle = (): Action<{}, AppState> => {
  console.log('setUpdateApiidle');
  return {
    type: ActionTypes.SET_UPDATE_CARD_STATUS,
    data: {updateCardstatus: APIStatus.idle},
  };
};
