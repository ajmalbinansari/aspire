import {Platform} from 'react-native';
import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {composeWithDevTools} from 'remote-redux-devtools';
import rootReducer from './reducers/root-reducer';
import dataSaga from './saga/root-saga';
import devTools from 'remote-redux-devtools';

const sagaMiddleware = createSagaMiddleware();
function configureStore() {
  const store = createStore(
    rootReducer,
    composeWithDevTools(
      applyMiddleware(sagaMiddleware),
      devTools({
        name: Platform.OS,
        hostname: 'localhost',
        port: 5678,
      }),
    ),
  );
  sagaMiddleware.run(dataSaga);
  return store;
}

export const store = configureStore();
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
