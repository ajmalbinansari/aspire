import {put} from 'redux-saga/effects';
import {getLimits, setWeeklyLimit} from '../../services/api-service';
import {DebitCard} from '../../types/debit-card';
import * as ActionTypes from '../actions/action-types';
import {APIStatus} from '../reducers/app-state';

function* fetchLimits(): Generator {
  try {
    yield put({
      type: ActionTypes.SET_LIMIT_API_LOADING,
      data: {fetchLimitsLoading: true},
    });
    const data = yield getLimits();
    yield put({type: ActionTypes.LIMITS_FETCHED, data: data});
    yield put({
      type: ActionTypes.SET_LIMIT_API_LOADING,
      data: {fetchLimitsLoading: false},
    });
  } catch (error) {
    yield put({
      type: ActionTypes.SET_LIMIT_API_LOADING,
      data: {fetchLimitsLoading: false},
    });
  }
}

function* setLimit({payload}: {payload: DebitCard; type: string}): Generator {
  try {
    yield put({
      type: ActionTypes.SET_UPDATE_CARD_API_LOADING,
      data: {updateCardLoading: true},
    });
    const data = yield setWeeklyLimit(payload);
    yield put({type: ActionTypes.DEBIT_CARD_UPDATED, data: data});
    yield put({
      type: ActionTypes.SET_UPDATE_CARD_STATUS,
      data: {updateCardstatus: APIStatus.done},
    });
    yield put({
      type: ActionTypes.SET_UPDATE_CARD_STATUS,
      data: {updateCardstatus: APIStatus.idle},
    });
    yield put({
      type: ActionTypes.SET_UPDATE_CARD_API_LOADING,
      data: {updateCardLoading: false},
    });
  } catch (error) {
    yield put({
      type: ActionTypes.SET_UPDATE_CARD_API_LOADING,
      data: {updateCardLoading: false},
    });
    yield put({
      type: ActionTypes.SET_UPDATE_CARD_STATUS,
      data: APIStatus.error,
    });
  }
}

export {fetchLimits, setLimit};
