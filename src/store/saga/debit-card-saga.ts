import {put} from 'redux-saga/effects';
import {getDebitCard} from '../../services/api-service';
import * as ActionTypes from '../actions/action-types';

function* fetchDebitCard(): Generator {
  try {
    yield put({
      type: ActionTypes.SET_DEBIT_CARD_API_LOADING,
      data: {fetchCardLoading: true},
    });
    const data = yield getDebitCard();
    yield put({type: ActionTypes.DEBIT_CARD_FETCHED, data: data});
    yield put({
      type: ActionTypes.SET_DEBIT_CARD_API_LOADING,
      data: {fetchCardLoading: false},
    });
  } catch (error) {
    yield put({
      type: ActionTypes.SET_DEBIT_CARD_API_LOADING,
      data: {fetchCardLoading: false},
    });
  }
}

export default fetchDebitCard;
