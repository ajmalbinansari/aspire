import {takeEvery} from 'redux-saga/effects';
import * as ActionTypes from '../actions/action-types';
import fetchDebitCard from './debit-card-saga';
import {fetchLimits, setLimit} from './limits-saga';

function* dataSaga() {
  yield takeEvery(ActionTypes.SET_LIMIT, setLimit);
  yield takeEvery(ActionTypes.GET_DEBIT_CARD, fetchDebitCard);
  yield takeEvery(ActionTypes.GET_LIMITS, fetchLimits);
}

export default dataSaga;
