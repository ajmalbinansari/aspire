import {combineReducers} from 'redux';
import AppStateReducer from './app-state';
import DebitCardReducer from './debit-card';
import LimitsReducer from './limits';

const RootReducer = combineReducers({
  debitCard: DebitCardReducer,
  limits: LimitsReducer,
  appState: AppStateReducer,
});

export default RootReducer;
export type IRootState = ReturnType<typeof RootReducer>;
