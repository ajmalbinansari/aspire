import Action from '../../types/redux-types';
import * as ActionTypes from '../actions/action-types';

export enum APIStatus {
  idle = 'idle',
  processing = 'processing',
  done = 'done',
  error = 'error',
}

export type AppState = {
  fetchCardLoading?: false;
  fetchLimitsLoading?: false;
  updateCardLoading?: false;
  updateCardstatus?: APIStatus;
};

const initialState: AppState = {
  fetchCardLoading: false,
  fetchLimitsLoading: false,
  updateCardLoading: false,
  updateCardstatus: APIStatus.idle,
};

const AppStateReducer = (
  state: AppState = initialState,
  action: Action<any, AppState>,
) => {
  switch (action.type) {
    case ActionTypes.SET_UPDATE_CARD_STATUS:
      return {...state, ...action.data};
    case ActionTypes.SET_UPDATE_CARD_API_LOADING:
      return {...state, ...action.data};
    case ActionTypes.SET_DEBIT_CARD_API_LOADING:
      return {...state, ...action.data};
    case ActionTypes.SET_LIMIT_API_LOADING:
      return {...state, ...action.data};
    default:
      return state;
  }
};

export default AppStateReducer;
