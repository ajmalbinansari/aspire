import Action from '../../types/redux-types';
import * as ActionTypes from '../actions/action-types';

const initialState: Array<number> = [];

const LimitsReducer = (
  state: Array<number> = initialState,
  action: Action<any, Array<number>>,
) => {
  switch (action.type) {
    case ActionTypes.GET_LIMITS:
      return {...state};
    case ActionTypes.LIMITS_FETCHED:
      return {...state, ...action.data};
    default:
      return state;
  }
};

export default LimitsReducer;
