import {DebitCard} from '../../types/debit-card';
import Action from '../../types/redux-types';
import * as ActionTypes from '../actions/action-types';

export const initialState: DebitCard = {
  availableBalance: 0,
  cardholderName: '',
  cardNumber: 0,
  expiry: '',
  cvv: 0,
  cardType: 'visa',
  spendingLimit: 0,
  spent: 0,
  isLimitSet: false,
};

const debitCardReducer = (
  state: DebitCard = initialState,
  action: Action<any, DebitCard>,
) => {
  switch (action.type) {
    case ActionTypes.GET_DEBIT_CARD:
      return {...state};
    case ActionTypes.DEBIT_CARD_FETCHED:
      return {...state, ...action.data};
    case ActionTypes.DEBIT_CARD_UPDATED:
      return {...state, ...action.data};
    default:
      return state;
  }
};

export default debitCardReducer;
