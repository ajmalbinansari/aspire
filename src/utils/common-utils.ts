import {splitEvery} from 'ramda';
import {Platform} from 'react-native';
import {CARD_WIDTH} from './constants';

export const numberWithCommas = (number: number) => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const removeCommas = (s: string) => {
  return s.replace(/,/g, '');
};

export const splitEvery4th = splitEvery(4);

export const splitCardNumber = (cardNumber: number) =>
  splitEvery4th(cardNumber.toString());

export const getRelativeSize = (width: number) => (size: number) => {
  return (width / CARD_WIDTH) * size;
};

export const isIos = Platform.OS === 'ios';
export const isAndroid = Platform.OS === 'android';

export const delay = (ms: number) => new Promise(res => setTimeout(res, ms));
