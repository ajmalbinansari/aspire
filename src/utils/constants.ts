export const BASE_DESIGN_WIDTH = 414;
export const BASE_DESIGN_HEIGHT = 736;
export const CARD_WIDTH = 366;
export const CARD_HEIGHT = 220;
export const CARD_SIZE_RATIO = CARD_HEIGHT / CARD_WIDTH;
export const MAX_CARD_WIDTH = 500;
