import {BASE_DESIGN_HEIGHT, BASE_DESIGN_WIDTH} from './constants';
import {isFinite} from 'lodash';
import {Dimensions} from 'react-native';

const {height, width} = Dimensions.get('window');

/**
 * Converts the pixel unit (e.g. value from design file) to a screen
 * width relative unit.
 *
 * @param unit value passed to be used as base value
 */
export function relativeWidth(unit: number, rounding = true) {
  const value = (unit / BASE_DESIGN_WIDTH) * width;
  return rounding ? Math.round(value) : value;
}

/**
 * Converts the pixel unit (e.g. value from design file) to a screen
 * height relative unit.
 *
 * @param unit value passed to be used as base value
 */
export function relativeHeight(unit: number) {
  const value = (unit / BASE_DESIGN_HEIGHT) * height;
  return Math.round(value);
}

/**
 * Converts the pixel unit (e.g. value from design file) to a screen
 * diagonal relative unit.
 *
 * @param unit value passed to be used as base value
 */
export function relativeDiagonal(unit: number, rounding = true) {
  const defaultHypotenuse = Math.sqrt(
    BASE_DESIGN_HEIGHT ** 2 + BASE_DESIGN_WIDTH ** 2,
  );
  const deviceHypotenuse = Math.sqrt(height ** 2 + width ** 2);
  const ratio = deviceHypotenuse / defaultHypotenuse;
  const value = ratio * unit;
  return rounding ? Math.round(value) : value;
}

/**
 * For greater control of sizing, scales the base design implementation to a desired factor.
 *
 * Very useful in creating consistent fontSizes.
 *
 * @param unit value passed to be used as base value
 */
export function scale(unit: number, factor = 0.7) {
  if (!isFinite(factor)) {
    return relativeDiagonal(unit);
  }
  return Math.round(unit + (relativeDiagonal(unit, false) - unit) * factor);
}
