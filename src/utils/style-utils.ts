import {TextStyle} from 'react-native';
import {fontFamilies} from '../components/asp-text/asp-text';
import {colors} from '../theme/colors';
import {scale} from './relative-sizes';

export const getFontStyle = (
  fontSize = scale(13),
  lineHeight = scale(12),
  fontFamily = fontFamilies.regular,
  color = colors.secondaryDark,
): TextStyle => ({
  fontSize: scale(fontSize),
  lineHeight: scale(lineHeight),
  color,
  fontFamily,
});
