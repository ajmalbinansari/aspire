import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {DebitCardScreen, SetLimitScreen} from '../../screens';

export type ScreenList = {
  DebitCardScreen: undefined;
  SetLimitScreen: undefined;
};
const Stack = createNativeStackNavigator<ScreenList>();

export const DebitCardStack = () => (
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <Stack.Screen name={'DebitCardScreen'} component={DebitCardScreen} />
    <Stack.Screen name={'SetLimitScreen'} component={SetLimitScreen} />
  </Stack.Navigator>
);
