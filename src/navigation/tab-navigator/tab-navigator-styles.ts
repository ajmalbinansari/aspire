import {Dimensions, StyleSheet} from 'react-native';
import {fontFamilies} from '../../components/asp-text/asp-text';
import {colors} from '../../theme/colors';
import {palette} from '../../theme/palette';
import {getFontStyle} from '../../utils/style-utils';

const {width} = Dimensions.get('window');

export default StyleSheet.create({
  tabBarLabelDisabled: {
    ...getFontStyle(9, 12, fontFamilies.medium, palette.grey1),
    marginLeft: width >= 768 ? 16 : 0,
  },
  tabBarLabel: {
    ...getFontStyle(9, 12, fontFamilies.demiBold, colors.primary),
    marginLeft: width >= 768 ? 16 : 0,
  },
});
