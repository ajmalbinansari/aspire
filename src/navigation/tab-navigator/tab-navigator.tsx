import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {DebitCardStack} from '../debit-card-stack/debit-card-stack';
import HomeTabIcon from '../../icons/home-tab-icon';
import {palette} from '../../theme/palette';
import {AspText as Text} from '../../components/asp-text/asp-text';
import styles from './tab-navigator-styles';
import DebitCardTabIcon from '../../icons/debit-card-tab-icon';
import {colors} from '../../theme/colors';
import PaymentsTabIcon from '../../icons/payments-tab-icons';
import CreditTabIcon from '../../icons/credit-tab-icon';
import ProfileTabIcon from '../../icons/profile-tab-icon';
import {useDispatch} from 'react-redux';
import {getDebitCard} from '../../store/actions/debit-card-actions';
import {Screen} from 'react-native-screens';

export type TabParamList = {
  HomeTab: undefined;
  DebitCardTab: undefined;
  PaymentsTab: undefined;
  CreditTab: undefined;
  ProfileTab: undefined;
};
const Tab = createBottomTabNavigator<TabParamList>();
const getTabLabel = (label: string, focused: boolean) => (
  <Text style={focused ? styles.tabBarLabel : styles.tabBarLabelDisabled}>
    {label}
  </Text>
);
const getTextColor = (focused: boolean) =>
  focused ? colors.primary : palette.grey1;

const tabPressListenerForDisabled = {
  // tabPress: (e: {preventDefault: () => any}) => e.preventDefault(),
};
export const TabNavigator = () => {
  const dispatch = useDispatch();
  return (
    <Tab.Navigator
      initialRouteName="DebitCardTab"
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen
        name={'HomeTab'}
        options={{
          tabBarLabel: ({focused}) => getTabLabel('Home', focused),
          tabBarIcon({focused}) {
            return <HomeTabIcon fill={getTextColor(focused)} />;
          },
        }}
        listeners={tabPressListenerForDisabled}>
        {() => <Screen />}
      </Tab.Screen>
      <Tab.Screen
        name={'DebitCardTab'}
        component={DebitCardStack}
        options={{
          tabBarLabel: ({focused}) => getTabLabel('Debit Card', focused),
          tabBarIcon({focused}) {
            return <DebitCardTabIcon fill={getTextColor(focused)} />;
          },
        }}
        listeners={{
          tabPress() {
            dispatch(getDebitCard());
          },
        }}
      />
      <Tab.Screen
        name={'PaymentsTab'}
        options={{
          tabBarLabel: ({focused}) => getTabLabel('Payments', focused),
          tabBarIcon: ({focused}) => {
            return <PaymentsTabIcon fill={getTextColor(focused)} />;
          },
        }}
        listeners={tabPressListenerForDisabled}>
        {() => <Screen />}
      </Tab.Screen>
      <Tab.Screen
        name={'CreditTab'}
        options={{
          tabBarLabel: ({focused}) => getTabLabel('Credit', focused),
          tabBarIcon({focused}) {
            return <CreditTabIcon fill={getTextColor(focused)} />;
          },
        }}
        listeners={tabPressListenerForDisabled}>
        {() => <Screen />}
      </Tab.Screen>
      <Tab.Screen
        name={'ProfileTab'}
        options={{
          tabBarLabel: ({focused}) => getTabLabel('Profile', focused),
          tabBarIcon({focused}) {
            return <ProfileTabIcon fill={getTextColor(focused)} />;
          },
        }}
        listeners={tabPressListenerForDisabled}>
        {() => <Screen />}
      </Tab.Screen>
    </Tab.Navigator>
  );
};
