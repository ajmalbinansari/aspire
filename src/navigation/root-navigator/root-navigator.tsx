import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {TabNavigator} from '../tab-navigator/tab-navigator';

export const RootNavigator = () => {
  return (
    <NavigationContainer>
      <TabNavigator />
    </NavigationContainer>
  );
};
