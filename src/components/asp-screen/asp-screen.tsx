import {NavigationProp} from '@react-navigation/native';
import React from 'react';
import {TouchableOpacity, View, ViewProps} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import HomeTabIcon from '../../icons/home-tab-icon';
import LeftArrow from '../../icons/left-arrow';
import {ScreenList} from '../../navigation/debit-card-stack/debit-card-stack';
import {colors} from '../../theme/colors';
import {scale} from '../../utils/relative-sizes';
import {AspSpinner} from '../asp-spinner/asp-spinner';
import styles from './asp-screen-styles';
export type AspScreenProps = ViewProps & {
  withLogo?: boolean;
  unsafe?: boolean;
  navigation?: NavigationProp<ScreenList>;
  showBackArrow?: boolean;
  isLoading?: boolean;
};
export const AspScreen = ({
  children,
  withLogo = false,
  unsafe = false,
  navigation,
  showBackArrow = false,
  isLoading = false,
  ...rest
}: AspScreenProps) => {
  const insets = useSafeAreaInsets();
  const insetStyle = {
    paddingTop: unsafe ? 0 : insets.top,
  };

  const onPressBack = () => {
    navigation?.canGoBack() && navigation.goBack();
  };
  return (
    <View style={[styles.screenStyle, insetStyle]} {...rest}>
      {navigation && showBackArrow && (
        <TouchableOpacity
          hitSlop={{
            top: scale(24),
            bottom: scale(24),
            left: scale(24),
            right: scale(24),
          }}
          onPress={onPressBack}
          style={styles.backArrowContainer}>
          <LeftArrow />
        </TouchableOpacity>
      )}
      {withLogo && (
        <View style={styles.topLogo}>
          <HomeTabIcon fill={colors.primary} />
        </View>
      )}
      {isLoading ? (
        <View style={styles.spinnerContainer}>
          <AspSpinner />
        </View>
      ) : (
        children
      )}
    </View>
  );
};
