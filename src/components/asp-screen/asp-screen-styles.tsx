import {StatusBar, StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {relativeHeight, relativeWidth} from '../../utils/relative-sizes';

export default StyleSheet.create({
  screenStyle: {
    flex: 1,
    backgroundColor: colors.secondary,
  },
  topLogo: {
    position: 'absolute',
    top: relativeHeight(36),
    right: relativeWidth(25),
  },
  backArrowContainer: {
    position: 'absolute',
    top: relativeHeight(36),
    left: relativeWidth(25),
    zIndex: 1,
  },
  contentContainer: {
    paddingTop: StatusBar.currentHeight,
    flex: 1,
  },
  spinnerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
