import React from 'react';
import {TouchableOpacity, View, ViewProps} from 'react-native';
import {AspText} from '../asp-text/asp-text';
import styles from './asp-button-styles';

export type AspButtonProps = ViewProps & {
  text: string;
  disabled?: boolean;
  onPress: () => void;
};
export const AspButton = ({
  text,
  disabled = false,
  onPress,
}: AspButtonProps) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={[
        styles.container,
        disabled ? styles.buttonDisabled : styles.buttonActive,
      ]}>
      <AspText style={styles.textStyle}>{text}</AspText>
    </TouchableOpacity>
  );
};
