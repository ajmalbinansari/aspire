import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {palette} from '../../theme/palette';
import {relativeHeight, relativeWidth, scale} from '../../utils/relative-sizes';
import {getFontStyle} from '../../utils/style-utils';
import {fontFamilies} from '../asp-text/asp-text';

export default StyleSheet.create({
  container: {
    width: relativeWidth(300),
    paddingVertical: relativeHeight(17),

    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(30),
  },
  textStyle: {
    ...getFontStyle(16, 20, fontFamilies.demiBold, palette.white),
  },
  buttonDisabled: {
    backgroundColor: palette.grey3,
  },
  buttonActive: {
    backgroundColor: colors.primary,
  },
});
