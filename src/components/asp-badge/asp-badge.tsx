import React from 'react';
import {TextStyle, View, ViewProps} from 'react-native';
import {AspText} from '../asp-text/asp-text';
import styles from './asp-badge-styles';

export type AspBadgeProps = ViewProps & {text: string; textStyle?: TextStyle};
export const AspBadge = ({style, text, textStyle}: AspBadgeProps) => {
  return (
    <View style={[styles.container, style]}>
      <AspText style={[styles.badgeText, textStyle]}>{text}</AspText>
    </View>
  );
};
