import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {palette} from '../../theme/palette';
import {relativeHeight, relativeWidth} from '../../utils/relative-sizes';
import {getFontStyle} from '../../utils/style-utils';
import {fontFamilies} from '../asp-text/asp-text';

export default StyleSheet.create({
  container: {
    minWidth: relativeWidth(40),
    paddingVertical: relativeHeight(3),
    paddingHorizontal: relativeWidth(3),
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  badgeText: {
    ...getFontStyle(12, 16, fontFamilies.bold, palette.white),
  },
});
