import React from 'react';
import {ActivityIndicator, ActivityIndicatorProps, View} from 'react-native';
import {colors} from '../../theme/colors';

export type AspSpinnerProps = ActivityIndicatorProps & {};
export const AspSpinner = (props: AspSpinnerProps) => {
  return (
    <View>
      <ActivityIndicator size="large" color={colors.primary} {...props} />
    </View>
  );
};
