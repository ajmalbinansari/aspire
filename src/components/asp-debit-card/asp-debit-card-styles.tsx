import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {palette} from '../../theme/palette';
import {scale} from '../../utils/relative-sizes';
import {getFontStyle} from '../../utils/style-utils';
import {fontFamilies} from '../asp-text/asp-text';

export default StyleSheet.create({
  cardContainer: {
    borderRadius: scale(12),
    backgroundColor: colors.primary,
    zIndex: 1,
  },
  aspireLogoContainer: {
    position: 'absolute',
  },
  nameContainer: {
    position: 'absolute',
  },
  name: {
    ...getFontStyle(22, 28, fontFamilies.bold, palette.white),
  },
  cardNumberContainer: {
    position: 'absolute',
  },
  expiryContainer: {
    position: 'absolute',
    flexDirection: 'row',
  },
  exp: {
    ...getFontStyle(13, 18, fontFamilies.demiBold, palette.white),
    letterSpacing: 1.56,
  },
  stars: {
    ...getFontStyle(24, 18, fontFamilies.demiBold, palette.white),
    top: 4,
  },
  cardTypeContainer: {
    position: 'absolute',
  },
  displayToggleContainer: {
    position: 'absolute',
    right: 1,
    backgroundColor: palette.white,
    borderTopLeftRadius: scale(6),
    borderTopRightRadius: scale(6),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  toggleText: {
    ...getFontStyle(12, 20, fontFamilies.demiBold, colors.primary),
    marginTop: -scale(6),
  },
  eyeIconContainer: {
    marginTop: -scale(6),
  },
  cvvContainer: {
    flexDirection: 'row',
  },
});
