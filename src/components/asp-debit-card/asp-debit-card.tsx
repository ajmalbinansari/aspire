import React from 'react';
import {TouchableOpacity, View, ViewProps} from 'react-native';
import AspireLogoText from '../../icons/aspire-logo-text';
import CrossEyeIcon from '../../icons/cross-eye-icon';
import EyeIcon from '../../icons/eye-icon';
import VisaLogo from '../../icons/visa-logo';
import {palette} from '../../theme/palette';
import {getRelativeSize} from '../../utils/common-utils';
import {CARD_SIZE_RATIO} from '../../utils/constants';
import {scale} from '../../utils/relative-sizes';
import {AspText} from '../asp-text/asp-text';
import {AspCardNumber} from '../asp-card-number/asp-card-number';
import styles from './asp-debit-card-styles';

export type DebitCardProps = ViewProps & {
  width: number;
  name: string;
  cardNumber: number;
  expiry: string;
  cvv: number;
  isNumberHidden: boolean;
  onPressNumberToggle: () => void;
};
export const AspDebitCard = ({
  width,
  name,
  cardNumber,
  expiry,
  cvv,
  isNumberHidden,
  onPressNumberToggle,
}: DebitCardProps) => {
  const relativeSize = getRelativeSize(width);
  return (
    <View>
      <TouchableOpacity
        onPress={onPressNumberToggle}
        style={[
          styles.displayToggleContainer,
          {
            width: scale(151),
            height: scale(44),
            top: -scale(32),
          },
        ]}>
        <View style={styles.eyeIconContainer}>
          {isNumberHidden ? <EyeIcon /> : <CrossEyeIcon />}
        </View>
        <AspText style={[styles.toggleText, {marginLeft: relativeSize(6)}]}>
          {`${isNumberHidden ? 'Show' : 'Hide'} card number`}
        </AspText>
      </TouchableOpacity>
      <View
        style={[
          {
            width,
            height: width * CARD_SIZE_RATIO,
          },
          styles.cardContainer,
        ]}>
        <View
          style={[
            styles.aspireLogoContainer,
            {right: relativeSize(24), top: relativeSize(24)},
          ]}>
          <AspireLogoText />
        </View>
        <View
          style={[
            styles.nameContainer,
            {
              left: relativeSize(24),
              top: relativeSize(69),
            },
          ]}>
          <AspText style={styles.name}>{name}</AspText>
        </View>
        <View
          style={[
            styles.cardNumberContainer,
            {left: relativeSize(24), bottom: relativeSize(80)},
          ]}>
          <AspCardNumber
            number={cardNumber}
            space={relativeSize(24)}
            isHidden={isNumberHidden}
          />
        </View>
        <View
          style={[
            styles.expiryContainer,
            {left: relativeSize(24), bottom: relativeSize(40)},
          ]}>
          <AspText style={styles.exp}>{`Thru: ${expiry}`}</AspText>
          <View style={[styles.cvvContainer, {marginLeft: relativeSize(32)}]}>
            <AspText style={styles.exp}>CVV:</AspText>
            <AspText style={isNumberHidden ? styles.stars : styles.exp}>{` ${
              isNumberHidden ? '∗∗∗' : cvv
            }`}</AspText>
          </View>
        </View>
        <View
          style={[
            styles.cardTypeContainer,
            {bottom: relativeSize(24), right: relativeSize(24)},
          ]}>
          <VisaLogo />
        </View>
      </View>
    </View>
  );
};
