import * as React from 'react';
import {Animated, Easing, Text, TouchableOpacity, View} from 'react-native';
import {colors} from '../../theme/colors';
import {palette} from '../../theme/palette';
import {scale} from '../../utils/relative-sizes';
import styles from './asp-switch-styles';

export type AspSwitchProps = {
  onColor?: string;
  offColor?: string;
  label?: string;
  onToggle: () => void;
  style?: object;
  isOn: boolean;
  labelStyle?: object;
};

export const AspSwitch = ({
  onColor = colors.primary,
  offColor = palette.grey3,
  label = '',
  onToggle = () => {},
  style = {},
  isOn = false,
  labelStyle = {},
}: AspSwitchProps) => {
  const animatedValue = React.useRef(new Animated.Value(0)).current;
  const moveToggle = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, scale(16)],
  });
  const color = isOn ? onColor : offColor;

  React.useEffect(() => {
    animatedValue.setValue(isOn ? 0 : 1);
    Animated.timing(animatedValue, {
      toValue: isOn ? 1 : 0,
      duration: 300,
      easing: Easing.linear,
      useNativeDriver: false,
    }).start();
  }, [animatedValue, isOn]);

  return (
    <View style={styles.container}>
      {!!label && <Text style={[styles.label, labelStyle]}>{label}</Text>}

      <TouchableOpacity
        onPress={() => {
          typeof onToggle === 'function' && onToggle();
        }}>
        <View style={[styles.toggleContainer, style, {backgroundColor: color}]}>
          <Animated.View
            style={[
              styles.toggleWheelStyle,
              {
                marginLeft: moveToggle,
              },
            ]}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};
