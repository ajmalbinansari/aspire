import {StyleSheet} from 'react-native';
import {scale} from '../../utils/relative-sizes';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  toggleContainer: {
    width: scale(34),
    height: scale(20),
    marginLeft: scale(3),
    borderRadius: scale(15),
    justifyContent: 'center',
  },
  label: {
    marginRight: scale(2),
  },
  toggleWheelStyle: {
    width: scale(16),
    height: scale(16),
    backgroundColor: 'white',
    borderRadius: scale(8),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: scale(2),
    },
    shadowOpacity: 0.2,
    shadowRadius: scale(2.5),
    elevation: 1.5,
  },
});
