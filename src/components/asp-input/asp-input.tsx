import React from 'react';
import {TextInput, TextInputProps, View} from 'react-native';
import {AspBadge} from '../asp-badge/asp-badge';
import styles from './asp-input-styles';

export type AspInputProps = TextInputProps & {};
export const AspInput = (props: AspInputProps) => {
  return (
    <View style={styles.container}>
      <AspBadge text={'S$'} />
      <TextInput style={styles.inputStyle} {...props} />
    </View>
  );
};
