import {StyleSheet} from 'react-native';
import {palette} from '../../theme/palette';
import {relativeHeight, relativeWidth} from '../../utils/relative-sizes';
import {getFontStyle} from '../../utils/style-utils';
import {fontFamilies} from '../asp-text/asp-text';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: palette.grey1,
    paddingTop: relativeHeight(4),
    paddingBottom: relativeHeight(11),
  },
  inputStyle: {
    flex: 1,
    marginLeft: relativeWidth(10),
    ...getFontStyle(24, 33, fontFamilies.bold, palette.grey2),
  },
});
