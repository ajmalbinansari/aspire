import React from 'react';
import {Text, TextProps} from 'react-native';

export enum FontFamilyTypes {
  regular = 'regular',
  medium = 'medium',
  demiBold = 'demiBold',
  bold = 'bold',
}
type AspTextProps = {
  family?: FontFamilyTypes;
};

export const fontFamilies: {[value in FontFamilyTypes]: string} = {
  regular: 'AvenirNext-Regular',
  medium: 'AvenirNext-Medium',
  demiBold: 'AvenirNext-DemiBold',
  bold: 'AvenirNext-Bold',
};
export const AspText = (props: TextProps & AspTextProps) => {
  const {family = FontFamilyTypes.regular, style, ...rest} = props;
  return (
    <Text
      allowFontScaling={false}
      style={[{fontFamily: fontFamilies[family]}, style]}
      {...rest}>
      {props.children}
    </Text>
  );
};
