import React from 'react';
import {View} from 'react-native';
import {AspText} from '../asp-text/asp-text';
import styles from './asp-progress-styles';

export type AspProgressProps = {
  completed: number;
  label: string;
  total: number;
};
export const AspProgress = ({label, completed, total}: AspProgressProps) => {
  return (
    <View>
      <View style={styles.labelContainer}>
        <AspText style={styles.labelStyle}>{label}</AspText>
        <AspText>
          <AspText style={styles.completedLabel}>{`$${completed}`}</AspText>
          <View style={styles.labelSeparatorContainer}>
            <AspText style={styles.labelSeparator}>|</AspText>
          </View>
          <AspText style={styles.totalLabel}>{`$${total}`}</AspText>
        </AspText>
      </View>
      {isFinite(completed / total) && isFinite((total - completed) / total) && (
        <View style={styles.progressContainer}>
          <View style={[styles.progressBox, {flex: completed / total}]} />
          <View style={{flex: (total - completed) / total}} />
        </View>
      )}
    </View>
  );
};
