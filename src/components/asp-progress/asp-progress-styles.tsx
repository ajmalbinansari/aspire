import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {palette} from '../../theme/palette';
import {relativeHeight, relativeWidth, scale} from '../../utils/relative-sizes';
import {getFontStyle} from '../../utils/style-utils';
import {fontFamilies} from '../asp-text/asp-text';

export default StyleSheet.create({
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  labelStyle: {
    ...getFontStyle(13, 18, fontFamilies.medium, palette.grey2),
  },
  completedLabel: {
    ...getFontStyle(13, 18, fontFamilies.demiBold, colors.primary),
  },
  totalLabel: {
    ...getFontStyle(13, 18, fontFamilies.demiBold, palette.grey4),
  },
  labelSeparator: {
    ...getFontStyle(13, 20, fontFamilies.demiBold, palette.grey4),
  },
  labelSeparatorContainer: {
    paddingHorizontal: relativeWidth(4),
  },
  progressContainer: {
    height: relativeHeight(15),
    borderRadius: scale(30),
    backgroundColor: palette.lightestGreen,
    marginTop: relativeHeight(6),
    flexDirection: 'row',
    overflow: 'hidden',
  },
  //   ##TODO skewX transform won't work in android. Might need some different approch in android to get the same result as in iOS
  // https://github.com/facebook/react-native/issues/27649
  progressBox: {
    backgroundColor: colors.primary,
    borderColor: colors.primary,
    transform: [{skewX: '-20deg'}],
  },
});
