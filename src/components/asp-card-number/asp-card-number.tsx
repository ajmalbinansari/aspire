import React from 'react';
import {View, ViewProps} from 'react-native';
import {splitCardNumber} from '../../utils/common-utils';
import {relativeWidth} from '../../utils/relative-sizes';
import {AspText} from '../asp-text/asp-text';
import styles from './asp-card-number-styles';

export type CardNumberProps = ViewProps & {
  number: number;
  space: number;
  isHidden: boolean;
};

export const AspCardNumber = ({number, space, isHidden}: CardNumberProps) => {
  const renderDots = (index: number) => {
    const marginLeft = index !== 0 ? space : 0;
    return (
      <View key={index.toString()} style={[styles.dotContainer, {marginLeft}]}>
        {[...Array(4).keys()].map(i => {
          const mLeft = i !== 0 ? relativeWidth(6) : 0;
          return (
            <View
              key={i.toString()}
              style={[styles.dot, {marginLeft: mLeft}]}
            />
          );
        })}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      {splitCardNumber(number).map((cardPart: string, i: number) => {
        const marginLeft = i !== 0 ? space : 0;
        return isHidden && i <= 2 ? (
          renderDots(i)
        ) : (
          <AspText
            key={i.toString()}
            style={[styles.numberStyle, {marginLeft}]}>
            {cardPart}
          </AspText>
        );
      })}
    </View>
  );
};
