import {StyleSheet} from 'react-native';
import {palette} from '../../theme/palette';
import {scale} from '../../utils/relative-sizes';
import {getFontStyle} from '../../utils/style-utils';
import {fontFamilies} from '../asp-text/asp-text';

const DOT_SIZE = 8;
export default StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  numberStyle: {
    ...getFontStyle(14, 19, fontFamilies.demiBold, palette.white),
    letterSpacing: 3.4,
  },
  dotContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dot: {
    width: scale(DOT_SIZE),
    height: scale(DOT_SIZE),
    borderRadius: scale(DOT_SIZE / 2),
    backgroundColor: palette.white,
  },
});
