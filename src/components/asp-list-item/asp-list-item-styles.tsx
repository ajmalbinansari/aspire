import {StyleSheet} from 'react-native';
import {colors} from '../../theme/colors';
import {palette} from '../../theme/palette';
import {relativeHeight, relativeWidth} from '../../utils/relative-sizes';
import {getFontStyle} from '../../utils/style-utils';
import {fontFamilies} from '../asp-text/asp-text';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: relativeHeight(17),
  },
  title: {
    ...getFontStyle(14, 19, fontFamilies.medium, colors.secondaryDark),
  },
  subTitle: {
    ...getFontStyle(13, 18, fontFamilies.regular, palette.grey2),
  },
  titlesContainer: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: relativeWidth(12),
  },
});
