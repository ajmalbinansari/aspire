import React from 'react';
import {TouchableOpacity, View, ViewProps} from 'react-native';
import {AspSwitch} from '../asp-switch/asp-switch';
import {AspText} from '../asp-text/asp-text';
import styles from './asp-list-item-styles';

export type ListItemProps = ViewProps & {
  title: string;
  subTitle?: string;
  hasSwitch?: boolean;
  isSwitchOn?: boolean;
  onToggleSwitch?: () => void;
  onPressItem?: () => void;
  leadingIcon?: Element;
};
export const AspListItem = ({
  leadingIcon,
  title,
  subTitle,
  hasSwitch = false,
  onToggleSwitch = () => null,
  onPressItem,

  isSwitchOn = false,
}: ListItemProps) => {
  const renderItem = () => {
    return (
      <View style={styles.container}>
        <View>{leadingIcon}</View>
        <View style={styles.titlesContainer}>
          <AspText style={styles.title}>{title}</AspText>
          <AspText style={styles.subTitle}>{subTitle}</AspText>
        </View>
        {hasSwitch && (
          <View>
            <AspSwitch isOn={isSwitchOn} onToggle={onToggleSwitch} />
          </View>
        )}
      </View>
    );
  };
  return onPressItem ? (
    <TouchableOpacity onPress={onPressItem}>{renderItem()}</TouchableOpacity>
  ) : (
    renderItem()
  );
};
