import {palette} from './palette';

export const colors = {
  primary: palette.green,
  secondary: palette.marine,
  secondaryDark: palette.darkMarine,
};
