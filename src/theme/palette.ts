export const palette = {
  grey1: '#dddddd',
  grey2: '#222222',
  grey3: '#EEEEEE',
  grey4: '#22222233',
  green: '#01D167',
  lightestGreen: '#E0FCF0',
  marine: '#0C365A',
  darkMarine: '#25345F',
  white: '#ffffff',
};
