import * as React from 'react';
import Svg, {Path, SvgProps} from 'react-native-svg';
import {palette} from '../theme/palette';
import {relativeHeight, relativeWidth} from '../utils/relative-sizes';

const LeftArrow = (props: SvgProps) => (
  <Svg
    width={relativeWidth(16)}
    height={relativeHeight(16)}
    viewBox="0 0 512.006 512.006"
    {...props}>
    <Path
      fill={palette.white}
      d="M388.419 475.59 168.834 256.005 388.418 36.421c8.341-8.341 8.341-21.824 0-30.165s-21.824-8.341-30.165 0L123.586 240.923c-8.341 8.341-8.341 21.824 0 30.165l234.667 234.667a21.275 21.275 0 0 0 15.083 6.251 21.277 21.277 0 0 0 15.083-6.251c8.341-8.341 8.341-21.824 0-30.165z"
    />
  </Svg>
);

export default LeftArrow;
