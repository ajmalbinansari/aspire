import {StyleSheet} from 'react-native';
import {fontFamilies} from '../../components/asp-text/asp-text';
import {palette} from '../../theme/palette';
import {relativeHeight, relativeWidth, scale} from '../../utils/relative-sizes';
import {getFontStyle} from '../../utils/style-utils';

export default StyleSheet.create({
  bgView: {
    marginTop: relativeHeight(80),
    paddingHorizontal: relativeWidth(24),
    flexDirection: 'column',
  },
  screenTitle: {
    ...getFontStyle(24, 32, fontFamilies.bold, palette.white),
  },
  overlayContentContainer: {
    minHeight: '100%',
    flex: 1,
    // alignItems: 'center',
    paddingHorizontal: relativeWidth(24),
    paddingTop: relativeHeight(32),
    backgroundColor: palette.white,
    borderTopLeftRadius: scale(24),
    borderTopRightRadius: scale(24),
    paddingBottom: relativeHeight(24),
  },
  scrollView: {
    ...StyleSheet.absoluteFillObject,
    top: relativeHeight(32),
  },
  title: {
    ...getFontStyle(14, 19, fontFamilies.medium, palette.grey2),
    marginLeft: relativeWidth(12),
  },
  titleContainer: {
    flexDirection: 'row',
  },
  iconStyle: {
    width: scale(16),
    height: scale(16),
  },
  subText: {
    ...getFontStyle(13, 18, fontFamilies.regular, palette.grey2),
    opacity: 0.33,
    marginTop: relativeHeight(12),
  },
  inputContainer: {
    marginTop: relativeHeight(13),
  },
  pillsRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  badgeStyle: {
    flex: 1,
    backgroundColor: palette.lightestGreen,
    paddingVertical: relativeHeight(12),
    marginHorizontal: relativeWidth(6),
  },
  touchableBadge: {
    flex: 1,
  },
  buttonContainer: {
    alignItems: 'center',
  },
  placeholder: {flex: 1},
  scrollViewBottomOffset: {height: relativeHeight(153)},
});
