import {NavigationProp, useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {Image, ScrollView, TouchableOpacity, View} from 'react-native';
import {AspScreen} from '../../components/asp-screen/asp-screen';
import {AspText, fontFamilies} from '../../components/asp-text/asp-text';
import {ScreenList} from '../../navigation/debit-card-stack/debit-card-stack';
import {relativeHeight} from '../../utils/relative-sizes';
import styles from './set-limit-screen-styles';
import LimitDark from '../../../assets/icons/limit-dark.png';
import {AspInput} from '../../components/asp-input/asp-input';
import {AspBadge} from '../../components/asp-badge/asp-badge';
import {getLimits, updateLimit} from '../../store/actions/limits-actions';
import {addIndex, identity, map, values} from 'ramda';
import {getFontStyle} from '../../utils/style-utils';
import {colors} from '../../theme/colors';
import {AspButton} from '../../components/asp-button/asp-button';
import {removeCommas} from '../../utils/common-utils';
import {AspSpinner} from '../../components/asp-spinner/asp-spinner';
import {APIStatus} from '../../store/reducers/app-state';
import {useAppDispatch, useAppSelector} from '../../hooks/hooks';

var mapIndexed = addIndex(map);
export const SetLimitScreen = () => {
  const navigation = useNavigation<NavigationProp<ScreenList>>();
  const [limit, setLimit] = useState('');
  const state = useAppSelector(identity);

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getLimits());
  }, [dispatch]);

  const onChangeText = (text: string) => {
    text
      ? setLimit(parseFloat(text.replace(/,/g, '')).toLocaleString('en'))
      : setLimit('');
  };

  const onPressSave = () => {
    dispatch(
      updateLimit({
        ...state.debitCard,
        isLimitSet: true,
        spendingLimit: parseFloat(removeCommas(limit)),
      }),
    );
  };

  useEffect(() => {
    if (state.appState.updateCardstatus === APIStatus.done) {
      navigation.goBack();
    }
  }, [dispatch, navigation, state.appState]);

  const onPressLimit = (l: number) => {
    onChangeText(l.toString());
  };

  const limitsArray = values(state.limits);
  return (
    <AspScreen withLogo unsafe navigation={navigation} showBackArrow>
      <View style={styles.bgView}>
        <AspText style={styles.screenTitle}>Spending limit</AspText>
      </View>

      <ScrollView style={styles.scrollView} bounces={false}>
        <View
          style={{
            height: relativeHeight(153),
          }}
        />
        <View style={styles.overlayContentContainer}>
          <View style={styles.titleContainer}>
            <Image source={LimitDark} style={styles.iconStyle} />
            <AspText style={styles.title}>
              {'Set a weekly debit card spending limit'}
            </AspText>
          </View>
          <View style={styles.inputContainer}>
            <AspInput
              keyboardType={'numeric'}
              value={limit}
              onChangeText={onChangeText}
              editable={false}
            />
          </View>
          <AspText style={styles.subText}>
            {'Here weekly means the last 7 days - not the calendar week'}
          </AspText>

          {state.appState.fetchLimitsLoading ? (
            <AspSpinner />
          ) : (
            <View style={styles.pillsRow}>
              {mapIndexed((l, i) => {
                const marginLeft = i === 0 ? 0 : undefined;
                const marginRight =
                  i === limitsArray.length - 1 ? 0 : undefined;
                return (
                  <TouchableOpacity
                    key={i.toString()}
                    style={styles.touchableBadge}
                    onPress={() => onPressLimit(l)}>
                    <AspBadge
                      text={`$${l}`}
                      style={[
                        styles.badgeStyle,
                        {
                          marginLeft,
                          marginRight,
                        },
                      ]}
                      textStyle={getFontStyle(
                        12,
                        16,
                        fontFamilies.demiBold,
                        colors.primary,
                      )}
                    />
                  </TouchableOpacity>
                );
              }, limitsArray)}
            </View>
          )}

          <View style={styles.placeholder} />

          <View style={styles.buttonContainer}>
            <AspButton
              text="Save"
              disabled={limit === '' || state.appState.updateCardLoading}
              onPress={onPressSave}
            />
          </View>
          <View style={styles.scrollViewBottomOffset} />
        </View>
      </ScrollView>
    </AspScreen>
  );
};
