import React, {useEffect, useState} from 'react';
import {
  Image,
  LayoutAnimation,
  LayoutChangeEvent,
  ScrollView,
  View,
} from 'react-native';
import {AspBadge} from '../../components/asp-badge/asp-badge';
import {AspScreen} from '../../components/asp-screen/asp-screen';
import {AspText} from '../../components/asp-text/asp-text';
import {AspDebitCard as DebitCardComponent} from '../../components/asp-debit-card/asp-debit-card';
import {AspListItem} from '../../components/asp-list-item/asp-list-item';
import TopUpIcon from '../../../assets/icons/topup.png';
import LimitIcon from '../../../assets/icons/limit.png';
import FreezeIcon from '../../../assets/icons/freeze.png';
import DeactivatedIcon from '../../../assets/icons/deactivated.png';
import GetNewIcon from '../../../assets/icons/getnew.png';
import {getDebitCard} from '../../store/actions/debit-card-actions';
import {getRelativeSize, numberWithCommas} from '../../utils/common-utils';
import {relativeHeight, relativeWidth} from '../../utils/relative-sizes';
import styles from './debit-card-screen-styles';
import {AspProgress} from '../../components/asp-progress/asp-progress';
import {NavigationProp, useNavigation} from '@react-navigation/native';
import {ScreenList} from '../../navigation/debit-card-stack/debit-card-stack';
import {updateLimit} from '../../store/actions/limits-actions';
import {RootState} from '../../store/root-store';
import {useAppDispatch, useAppSelector} from '../../hooks/hooks';

const CARD_WIDTH = relativeWidth(366);
const CARD_OFFSET = getRelativeSize(CARD_WIDTH)(60);
interface ListItem {
  title: string;
  subTitle?: string;
  hasSwitch?: boolean;
  isSwitchOn?: boolean;
  onToggleSwitch?: () => void;
  onPressItem?: () => void;
  leadingIcon?: Element;
}
export const DebitCardScreen = () => {
  const dispatch = useAppDispatch();
  const [bgContentHeight, setBgContentHeight] = useState(0);
  const [isCardNumberHidden, setIscardNumberHidden] = useState(false);
  const navigation = useNavigation<NavigationProp<ScreenList>>();

  const {debitCard, appState} = useAppSelector(s => s);
  const [hasLimit, setHasLimit] = useState(debitCard.isLimitSet);
  useEffect(() => {
    dispatch(getDebitCard());
  }, [dispatch]);

  const onPressNumberToggle = () => {
    setIscardNumberHidden(!isCardNumberHidden);
  };
  const onPressLimitSwitch = () => {
    if (debitCard.isLimitSet) {
      dispatch(
        updateLimit({...debitCard, spendingLimit: 0, isLimitSet: false}),
      );
    } else {
      navigation.navigate('SetLimitScreen');
    }
  };

  useEffect(() => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setHasLimit(debitCard.isLimitSet);
  }, [debitCard.isLimitSet]);

  const itemsArray: Array<ListItem> = [
    {
      title: 'Top-up account',
      subTitle: 'Deposit money to your account to use with card',
      leadingIcon: <Image source={TopUpIcon} style={styles.iconStyle} />,
    },
    {
      title: 'Weekly spending limit',
      subTitle: 'Your weekly spending limit is S$ 5,000',
      hasSwitch: true,
      isSwitchOn: hasLimit,
      onToggleSwitch: onPressLimitSwitch,
      onPressItem: onPressLimitSwitch,
      leadingIcon: <Image source={LimitIcon} style={styles.iconStyle} />,
    },
    {
      title: 'Freeze card',
      subTitle: 'Your debit card is currently active',
      hasSwitch: true,
      isSwitchOn: false,
      onToggleSwitch: () => null,
      leadingIcon: <Image source={FreezeIcon} style={styles.iconStyle} />,
    },
    {
      title: 'Get a new card',
      subTitle: 'This deactivates your current debit card',
      leadingIcon: <Image source={GetNewIcon} style={styles.iconStyle} />,
    },
    {
      title: 'Deactivated cards',
      subTitle: 'Your previously deactivated cards',
      leadingIcon: <Image source={DeactivatedIcon} style={styles.iconStyle} />,
    },
  ];

  const onLayoutChange = (e: LayoutChangeEvent) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setBgContentHeight(e.nativeEvent.layout.height);
  };

  return (
    <AspScreen withLogo unsafe isLoading={appState.fetchCardLoading}>
      <View style={styles.bgView} onLayout={onLayoutChange}>
        <AspText style={styles.screenTitle}>Debit Card</AspText>
        <AspText style={styles.screenSub}>Available balance</AspText>
        <View style={styles.balanceRow}>
          <AspBadge text={'S$'} style={styles.badgeStyleOverride} />
          <AspText style={styles.balanceAmount}>
            {numberWithCommas(debitCard.availableBalance)}
          </AspText>
        </View>
      </View>

      <ScrollView style={styles.scrollView} bounces={false}>
        <View
          style={{
            height: bgContentHeight + relativeHeight(52) + relativeHeight(2),
          }}
        />
        <View style={[styles.overlayContentContainer, {top: CARD_OFFSET}]}>
          <View style={{top: -CARD_OFFSET}}>
            <DebitCardComponent
              width={CARD_WIDTH}
              name={debitCard.cardholderName}
              cardNumber={debitCard.cardNumber}
              expiry={debitCard.expiry}
              cvv={debitCard.cvv}
              isNumberHidden={isCardNumberHidden}
              onPressNumberToggle={onPressNumberToggle}
            />

            {hasLimit && (
              <View style={styles.progressContainer}>
                <AspProgress
                  label="Debit card spending limit"
                  completed={debitCard.spent}
                  total={debitCard.spendingLimit}
                />
              </View>
            )}

            <View style={styles.listContainer}>
              {itemsArray.map((item: ListItem) => (
                <AspListItem key={item.title} {...item} />
              ))}
            </View>
          </View>
        </View>
      </ScrollView>
    </AspScreen>
  );
};
