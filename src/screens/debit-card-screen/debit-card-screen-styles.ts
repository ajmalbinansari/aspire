import {StyleSheet} from 'react-native';
import {fontFamilies} from '../../components/asp-text/asp-text';
import {palette} from '../../theme/palette';
import {relativeHeight, relativeWidth, scale} from '../../utils/relative-sizes';
import {getFontStyle} from '../../utils/style-utils';

export default StyleSheet.create({
  scrollView: {
    ...StyleSheet.absoluteFillObject,
    top: relativeHeight(32),
  },
  screenTitle: {
    ...getFontStyle(24, 32, fontFamilies.bold, palette.white),
  },
  screenSub: {
    ...getFontStyle(14, 16, fontFamilies.medium, palette.white),
    marginTop: relativeHeight(24),
  },
  bgView: {
    marginTop: relativeHeight(52),
    paddingHorizontal: relativeWidth(24),
    flexDirection: 'column',
  },
  balanceRow: {
    flexDirection: 'row',
    marginTop: relativeHeight(15),
  },
  badgeStyleOverride: {
    marginRight: relativeWidth(10),
  },
  balanceAmount: {
    ...getFontStyle(24, 32, fontFamilies.bold, palette.white),
  },
  overlayContentContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: palette.white,
    borderTopLeftRadius: scale(24),
    borderTopRightRadius: scale(24),
    paddingBottom: relativeHeight(24),
  },
  iconStyle: {width: scale(32), height: scale(32)},
  progressContainer: {
    marginTop: relativeHeight(26),
  },
  listContainer: {
    marginTop: relativeHeight(34),
  },
});
