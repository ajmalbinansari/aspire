# Assignment

This is an assignment project which is to build the given design

* This project using relative sizes for all dimensions so that the design will scale according to the display size.
* For icons this projects rely hugely on SVG assets converted into RN Components using `react-native-svg` even though it uses png assets where the svg assets doesn't seems to work properly.


## API Endpoints
 1. [GET] `/debitCard` returns a single debit card
```json
{
    "availableBalance": 768546,
    "cardholderName": "John Doe",
    "cardNumber": 6777987645670934,
    "expiry": "12/26",
    "cvv": 987,
    "cardType": "visa",
    "spendingLimit": 10000,
    "spent": 1000,
    "isLimitSet": true
  }
```
2. [GET] `/weeklylimits` returns a list of amounts that can be use to set as weekly limit of the debit card
```json
[
  5000,
  10000,
  20000
]
```
3. [PUT] `/debitCard` to update a debit card. it requires all the fields in a debit card to update

## Installation

From the root of the project use npm to install the dependencies

```bash
npm install
```

## Start development server

This project uses [json-server](https://www.npmjs.com/package/json-server) for development apis.

Up the server. 
```bash
json-server --watch db.json
```
The db.json file is located at the root of the project


## Setup `.env`
Create a `.env` file at the root of the project and add the variable in it. The required variables can be found in the `.env.example` file. `API_BASE_URL` would be typically `http://localhost:3000` if we use `json-server`


## Running the app

To run the project in android, use 
```bash
npm run android
```

To run the project in ios, use 
```bash
npm run ios
```

## Tests
Sample unit tests are added for apis, utils, and reducers

## Screens included
1. debit-card-screen
2. set-limit-screen

## Components included
1. asp-badge
2. asp-button
3. asp-card-number
4. asp-debit-card
5. asp-input
6. asp-list-item
7. asp-progress
8. asp-screen
9. asp-spinner
10. asp-switch
11. asp-text

