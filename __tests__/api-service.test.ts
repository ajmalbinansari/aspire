import {getDebitCard} from '../src/services/api-service';

const mockDebitCardResponse = {
  availableBalance: 768546,
  cardholderName: 'John Doe',
  cardNumber: 6777987645670934,
  expiry: '12/26',
  cvv: 987,
  cardType: 'visa',
  spendingLimit: 5000,
  spent: 1000,
  isLimitSet: true,
};

describe('Tests all apis returns the expected value', () => {
  it('returns correct for debitcard api', async () => {
    fetch = jest.fn(() =>
      Promise.resolve({
        ok: true,
        status: 200,
        json: () => mockDebitCardResponse,
      }),
    );
    const data = await getDebitCard();
    expect(data).toBe(mockDebitCardResponse);
  });

  it('throws exception if response is not ok', async () => {
    fetch = jest.fn(() =>
      Promise.resolve({
        ok: false,
        status: 404,
        json: () => '',
      }),
    );
    await expect(getDebitCard()).rejects.toThrow('Cannot get data');
  });
});
