import {Dimensions} from 'react-native';
import {scale} from '../src/utils/relative-sizes';

// Dimension under test env is {width:750, height: 1334}
describe('This block tests the relative sizes util functions used for size scaling', () => {
  it('confirms the dimension under test is expected', () => {
    const {width, height} = Dimensions.get('window');
    expect(height).toBe(1334);
    expect(width).toBe(750);
  });
  it('scale function returns proper value', () => {
    expect(scale(100)).toBe(157);
    expect(scale(200)).toBe(314);
    expect(scale(150)).toBe(235);
  });
});
