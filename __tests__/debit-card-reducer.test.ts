import debitCardReducer, {initialState} from '../src/store/reducers/debit-card';

it('should return the initial state', () => {
  expect(debitCardReducer(undefined, {})).toEqual(initialState);
});
